var cities = []
var smallDataset = { "Austin": {"lat":30,"lon":-97}, "Houston": {"lat":30,"lon":-95}, "Dallas":{"lat":33,"lon":-97}};

function createCity(){
    var city = document.getElementById("cityName").value;
    if(city in smallDataset) {
        var newDiv = document.createElement("div");
        newDiv.appendChild(document.createTextNode(city));
        document.getElementById("cities").appendChild(newDiv);
        var size = cities.push(city);
        document.getElementById("warning").style.display = "none";
        updateNumOfCities(size);
        if(cities.length >= 2) {
            getNewPath();
        }
    } else {
        document.getElementById("warning").style.display = "inline";
    }
    document.getElementById("cityName").value = "";
}

function search(ele) {
    if(event.keyCode == 13) {
        createCity();        
    }
}

function getNewPath(){
    console.log("Running getNewPath");
    bruteForceRecursive([], cities, 0);
    var path = shortestPath;
    document.getElementById("pathLength").innerHTML = path[path.length - 1];
    document.getElementById("path").innerHTML = path.slice(0,path.length - 1);
}

var shortestPath = [Number.MAX_VALUE];

function bruteForceRecursive(currentPath, nodesLeft) {
    if(nodesLeft.length == 0) {
        var pathLength = 0;
        for(var i = 0; i < currentPath.length - 1; i++) {
            pathLength += distance(currentPath[i], currentPath[i+1]);
        }
        var x = currentPath.push(pathLength);
        if(pathLength < shortestPath[shortestPath.length - 1]) {
            shortestPath = currentPath;
        }
    }
    else {
        for(var i = 0; i < nodesLeft.length; i++) {
            var newCurrentPath = deepCopy(currentPath);
            newCurrentPath.push(nodesLeft[i]);
            var newNodesLeft = deepCopy(nodesLeft);
            newNodesLeft.splice(i,1);
            bruteForceRecursive(newCurrentPath, newNodesLeft);
        }
    }
}

function deepCopy(a){
    var result = [];
    for(var i = 0; i < a.length; i++){
        result.push(a[i]);
    }
    return result;
}

function distance(cityA, cityB) {
    var Alon = smallDataset[cityA].lon;
    var Alat = smallDataset[cityA].lat;
    var Blon = smallDataset[cityB].lon;
    var Blat = smallDataset[cityB].lat;
    return haversine(Alon,Alat,Blon,Blat);
}

Number.prototype.toRad = function(){
    return this * Math.PI / 180;
}

function haversine(alon,alat,blon,blat){
    var R = 6371;
    var dLat = (blat - alat).toRad();
    var dLon = (blon - alon).toRad();
    var a = Math.sin(dLat / 2) * (Math.sin(dLat/2)) + Math.cos(alat.toRad()) * Math.cos(blat.toRad()) * Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return d;
}


function updateNumOfCities(size) {
    document.getElementById("numOfCities").innerHTML = size;
}


//bruteForceRecursive([], ["Austin", "Houston", "Dallas"]);
//console.log("Best Solution Found! Its: " + shortestPath);