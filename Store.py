'''
This is a script to produce a JSON file, of all cities in Texas,
with the distances between all the cities in Texas in dictionary form
Example (Orders are different anytime, because dictionary is formed in hash)
{
    Austin: {Austin: 0, City1: n1, City2: n2,...,CityN:nn},
    Dallas: {Austin: someNumber, Dallas: 0},
    ...
}
'''
#Scanning each line
#Split them up by ","
#Get the lower and upper name

import pprint
from haversine import haversine

cityDic = {}
txDic = {}

def Store_read(line):
    a = line.split(',')
    return [a[1],a[5],a[6]]

def Store_city(fileName):
    line = fileName.readline()
    while line != '':
        n,lat,lon = Store_read(line)
        cityDic[n] = [lat,lon]
        line = f.readline()

'''
Storing the distance for each cities as a dictionary
Each item from each cities is another dictionary,
containing range of distances from each cities
For exmaple (Order would be different)
Given Cities: A, B, C, D
cityDic would be
{
    A: {A:values, B:value, C:values, D:values},
    B: {A:values, B:value, C:values, D:values},
    C: {A:values, B:value, C:values, D:values},
    D: {A:values, B:value, C:values, D:values}

}
'''
def Store_dis(dic={}):
    for city in dic:
        txDic[city] = {}
        '''
        txDic: { city0: {}, city1: {}  }
        '''
        cityLat = float(dic[city][0])
        cityLon = float(dic[city][1])
        cityPt = (cityLat,cityLon)

        for cmpCity in dic:
            txDic[city][cmpCity] = 0
            '''
            txDic: { city: {cmpcity: 0 } }
            '''
            cmpCityLat = float(dic[cmpCity][0])
            cmpCityLon = float(dic[cmpCity][1])
            cmpCityPt = (cmpCityLat,cmpCityLon)
            disBw = getDis(cityPt,cmpCityPt)
            txDic[city][cmpCity] = disBw
            '''
            txDic: { city: {cmpcity: someNumber } }
            '''
            # txDic[city].update({cmpCity:disBw})
    return txDic



'''
Return distance in the unit of km,
by using havesine fomula (The great circle formula)
'''
def getDis(city1,city2):
    return haversine(city1,city2)

'''
Main function
'''
f = open('txcitiespop.txt', 'r')
f.readline()
Store_city(f)
txDic = Store_dis(cityDic)

# pp = pprint.PrettyPrinter(indent=4)
# pp.pprint(txDic)
